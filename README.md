## Re2o - Printer

This service uses Re2o API to manage printer jobs.


## Requirements

* python3
* requirements in https://gitlab.federez.net/re2o/re2oapi
