#!/usr/bin/env python3
#-*- mode: utf-8 -*-

from configparser import ConfigParser
import socket

import sys, os

from re2oapi import Re2oAPIClient

import subprocess

import argparse

DEBUG = False
VERBOSE = False

# LOCATION = '/re2o/media/'
LOCATION = '/var/impressions/'

path = os.path.dirname(os.path.abspath(__file__))
config = ConfigParser()
config.read(path+'/config.ini')

api_hostname = config.get('Re2o', 'hostname')
api_password = config.get('Re2o', 'password')
api_username = config.get('Re2o', 'username')

api_client = Re2oAPIClient(api_hostname, api_username, api_password, use_tls=False)

client_hostname = socket.gethostname().split('.', 1)[0]


class InvalidPDF(Exception):
    """
    """
    def __init__(self, message):
        super().__init__(message)


def get_path(filefield):
    """
    """
    return(LOCATION + filefield)

def HP_name_staples(name):
    """
    """
    if 'Top' in name:
        return '1Staple%sAngled' % (name.replace('Top', ''))
    else:
        return '2Staples%s' % name.split('Sided')[0]

def HP_name_punch(name):
    """
    """
    if name == "TwoLeftSidedHoles":
        return "2HolePunchLeft"
    if name == "TwoRightSidedHoles":
        return "2HolePunchRight"
    if name == "TwoTopHoles":
        return "2HolePunchTop"
    if name == "TwoBottomHoles":
        return "2HolePunchBottom"
    if name == "FourLeftSidedHoles":
        return "4HolePunchLeft"
    if name == "FourRightSidedHoles":
        return "4HolePunchRight"


def send(filepath, filename, number, pages, settings, debug, verbose):
    """
    """

    if debug:
        verbose = True

    filename = filename.replace(' ', '_')

    booklet = (settings['disposition'] == 'Booklet')

    cmd = ['lp', '-d', 'MFPM880-2', '-t', filename, '-n', str(number)]

    cmd += ['-o', 'Collate=True']

    if settings['format'] == 'A3':
        cmd += ['-o', 'pdf-expand',
                '-o', 'pdf-paper=841x1190'
                ]
    cmd += ['-o', 'PageSize=%s' % settings['format']]

    # Resize (avoid deadlock)
    cmd += ['-o', 'fit-to-page']

    # Default : print in color
    if settings['color'] == 'Greyscale':
        cmd += ['-o', 'HPColorAsGray=True',
                '-o', 'ColorModel=Grayscale']

    if booklet:
        cmd += ['-o', 'number-up=2']


    # Manage disposition
    if not (settings['disposition'] == 'OneSided'):

        if booklet:
            cmd += ['-o', 'sides=two-sided-short-edge']
        else:
            cmd += ['-o', 'sides=two-sided-long-edge']

    else:
        cmd += ['-o', 'sides=one-sided']

    # Staples
    stapling = settings['stapling']

    if booklet:
        cmd += ['-o', 'HPStaplerOptions=FoldStitch']

    elif not (stapling == 'None'):
        cmd += ['-o', 'HPStaplerOptions=%s' % HP_name_staples(stapling)]

    perforation = settings['perforation']

    # Holes
    if not booklet:
        if not(perforation == 'None'):
            cmd += ['-o', 'HPPunchingOptions=%s' % HP_name_punch(perforation)]


    # Format booklet
    if booklet:
        filepath = pdfbook(filepath, pages)

    # The End

    cmd.append('--')
    cmd.append(filepath)
    toprint = ''
    for k in cmd:
        toprint += k
        toprint += ' '

    if debug:
        print(toprint[:-1])
    else:
        if verbose:
            print(toprint[:-1])
        try:
            subprocess.check_output(cmd, stderr=subprocess.STDOUT)
        except subprocess.CalledProcessError as err:
            print(
                "Erreur lors de l'envoi du fichier %s à l'imprimante.\n"
                "Code de retour: %s, Sortie:\n%s",
                filename, err.returncode, err.output.decode()
            )
            print(err)


def extract(job):
    """
    """
    opts = ['color', 'disposition', 'format', 'perforation', 'stapling']
    options = {}
    for opt in opts:
        options[opt] = job[opt]
    filepath = get_path(job['file'])
    filename = job['filename']
    number = job['count']
    pages = job['pages']
    return(filepath, filename, number, pages, options)

import math
import itertools

def booklet_order(n):
    page = (lambda k: None if k > n else k)

    recto = 2*int(math.ceil(n/4.))
    verso = recto + 2

    while recto > 1:
        yield page(verso)
        yield page(recto-1)
        yield page(recto)
        yield page(verso-1)
        recto -= 2
        verso += 2

def pdfjam_order(n):
    return ",".join(map(lambda k: str(k or '{}'), booklet_order(n)))



def pdfbook(filepath, pages):
    """
    Creates a booklet from a pdf
    requires texlive-extra-utils
    """
    _dir = os.path.dirname(filepath)
    _fname = os.path.basename(filepath)
    newfile = os.path.join(_dir, "pdfbook_%s" % (_fname,))
    subprocess.check_output(
        ['/usr/bin/pdfjam',
         '--no-landscape',
         filepath,
         pdfjam_order(pages),
         '-o', newfile,
         ])
    return newfile

def check(filepath):
    """
    Check if the pdf is valid according to PDF spec.
    Requires qpdf
    """
    proc = subprocess.Popen(['qpdf', '--check', filepath],
                            stdout=subprocess.PIPE,
                            stderr=subprocess.PIPE)
    res = proc.communicate()
    # if proc.returncode:
    #     raise InvalidPDF(res[0].decode('utf-8').split('\n')[-2])


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Gestion des jobs à envoyer à l'imprimante")
    parser.add_argument('-d', '--debug', help="N'envoie pas les jobs à l'imprimante. Implique verbose.", action='store_true')
    parser.add_argument('-v', '--verbose', help="Affiche les actions sur stdout", action="store_true")
    parser.add_argument('-H', '--history', help="Récupère les anciens jobs", action='store_true')

    args = parser.parse_args()


    if args.debug:
        DEBUG = True
        VERBOSE = True

    if args.verbose:
        VERBOSE = True

    if args.history:
        for job in api_client.list("printer/history-jobs"):
            filepath, filename, number, pages, options = extract(job)
            try:
                check(filepath)
                send(filepath, filename, number, pages, options, debug=DEBUG, verbose=VERBOSE)
                print('\n')
            except InvalidPDF as e:
                print('Invalid PDF. Please resend a well formatted PDF')
                print(e)
                print('\n')

    else:
        for job in api_client.list("printer/printable-jobs"):
            filepath, filename, number, pages, options = extract(job)
            try:
                check(filepath)
                send(filepath, filename, number, pages, options, debug=DEBUG, verbose=VERBOSE)
                print('\n')
                api_client.patch(job['api_url'], data={'status': 'Running'})
            except InvalidPDF as e:
                print('Invalid PDF. Please resend a well formatted PDF')
                print(e)
                print('\n')
                api_client.patch(job['api_url'], data={'status': 'Cancelled'})
                print('Printing of %s Cancelled' % (filename,))
